export type TProps = {
  index: number;
  score?: number;
  avatar?: string;
  name?: string;
  login?: string;
  city?: string;
};
