export type TInitialFields = {
  topic: string;
  content: string;
};

export type TItem = {
  id: number;
  data: number;
  title: string;
  content: string;
};
