import { TItem } from './types';

export const MOCK_DATA: TItem[] = [
  {
    id: 1,
    data: 1645624227857,
    title: 'How to patch KDE on FreeBSD?',
    content:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Consequat aliquet maecenas ut sit nulla',
  },
  {
    id: 2,
    data: 1645624227857,
    title: 'How to patch KDE on FreeBSD?',
    content:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Consequat aliquet maecenas ut sit nulla',
  },
  {
    id: 3,
    data: 1645624227857,
    title: 'How to patch KDE on FreeBSD?',
    content:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Consequat aliquet maecenas ut sit nulla',
  },
  {
    id: 4,
    data: 1645624227857,
    title: 'How to patch KDE on FreeBSD?',
    content:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Consequat aliquet maecenas ut sit nulla',
  },
  {
    id: 5,
    data: 1645624227857,
    title: 'How to patch KDE on FreeBSD?',
    content:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Consequat aliquet maecenas ut sit nulla',
  },
];
