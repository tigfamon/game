export type TInitialFields = {
  message: string;
};

export type TMessage = {
  id: number;
  login: string;
  text: string;
  data: number;
};
